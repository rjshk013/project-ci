const express = require('express');
const app = express();
const port = process.env.NODE_PORT || 3000;
app.get('/', function (req, res) {
  res.send('Welcome to sample node js application!');
});
app.listen(port, function () {
  console.log('Listening on port ' + port);
});
